document.addEventListener('DOMContentLoaded', function() {  
    var audioPlayer = document.getElementById('audioPlayer');  
    var timerId;  
    var stayTime = 0;  
  
    // 重置计时器  
    function resetTimer() {  
        clearTimeout(timerId);  
        stayTime = 0;  
        timerId = setTimeout(playVoice, 15000); // 15秒 = 15000毫秒  
    }  
  
    // 播放语音  
    function playVoice() {  
        audioPlayer.play().catch(function(error) {  
            console.error('播放语音时出错:', error);  
        });  
    }  
  
    // 当用户与页面交互时重置计时器  
    document.addEventListener('mousemove', resetTimer);  
    document.addEventListener('keydown', resetTimer);  
    document.addEventListener('scroll', resetTimer);  
    document.addEventListener('click', resetTimer);  
  
    // 初始计时器  
    timerId = setTimeout(playVoice, 15000); // 初始计时器也设置为15秒  
});